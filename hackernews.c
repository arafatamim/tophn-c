/*
  *******************
  *  Run this file  *
  *******************
  tcc -lcurl -ljson-c -run hackernews.c
*/

#include <curl/curl.h>
#include <json-c/json.h>
#include <stdio.h>
#include <string.h>

struct string {
  char *ptr;
  size_t len;
};

struct story {
  const char *title;
  const char *url;
  const char *by;
  int32_t score;
};

void json_parse_top_five_ids(int *keys, json_object *jobj) {
  json_object *arr_item_obj;

  for (int i = 0; i < 5; i++) {
    arr_item_obj = json_object_array_get_idx(jobj, i);
    const char *item_str = json_object_get_string(arr_item_obj);
    keys[i] = atoi(item_str);
  }
}

void json_parse_story(struct story *story, json_object *jobj) {
  json_object *title_obj, *score_obj, *url_obj, *by_obj;
  const char *title_str, *url_str, *by_str;
  int32_t score_int;

  title_obj = json_object_object_get(jobj, "title");
  url_obj = json_object_object_get(jobj, "url");
  by_obj = json_object_object_get(jobj, "by");
  score_obj = json_object_object_get(jobj, "score");

  title_str = json_object_get_string(title_obj);
  url_str = json_object_get_string(url_obj);
  by_str = json_object_get_string(by_obj);
  score_int = json_object_get_int(score_obj);

  story->title = title_str;
  story->url = url_str;
  story->by = by_str;
  story->score = score_int;
}

void init_string(struct string *s) {
  s->len = 0;
  s->ptr = malloc(s->len + 1);
  if (s->ptr == NULL) {
    fprintf(stderr, "malloc() failed\n");
    exit(EXIT_FAILURE);
  }
  s->ptr[0] = '\0';
}

size_t writefunc(void *ptr, size_t size, size_t nmemb, struct string *s) {
  size_t new_len = s->len + size * nmemb;
  s->ptr = realloc(s->ptr, new_len + 1);
  if (s->ptr == NULL) {
    fprintf(stderr, "realloc() failed\n");
    exit(EXIT_FAILURE);
  }
  memcpy(s->ptr + s->len, ptr, size * nmemb);
  s->ptr[new_len] = '\0';
  s->len = new_len;
  return size * nmemb;
}

int main(void) {
  CURL *curl;
  CURLcode code;
  curl = curl_easy_init();
  if (curl) {
    struct string s;
    init_string(&s);

    curl_easy_setopt(curl, CURLOPT_URL,
                     "https://hacker-news.firebaseio.com/v0/topstories.json");
    curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, 1L);
    curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, writefunc);
    curl_easy_setopt(curl, CURLOPT_WRITEDATA, &s);
    code = curl_easy_perform(curl);

    /* if (res != CURLE_OK) {
      fprintf(stderr, "request failed!");
    } */

    json_object *jobj;
    jobj = json_tokener_parse(s.ptr);
    free(s.ptr);

    int keys[5];
    json_parse_top_five_ids(keys, jobj);

    for (int i = 0; i < 5; i++) {
      struct string res;
      init_string(&res);
      char story_url[60];
      sprintf(story_url, "https://hacker-news.firebaseio.com/v0/item/%d.json",
              keys[i]);

      curl_easy_setopt(curl, CURLOPT_URL, story_url);
      curl_easy_setopt(curl, CURLOPT_WRITEDATA, &res);
      code = curl_easy_perform(curl);

      jobj = json_tokener_parse(res.ptr);
      free(res.ptr);

      struct story story;
      json_parse_story(&story, jobj);

      printf("%s\n", story.title);
      printf("Score: %d\n", story.score);
      printf("By: %s\n", story.by);
      printf("URL: %s\n", story.url);
      printf("\n");
    }

    curl_easy_cleanup(curl);
  }

  return 0;
}
